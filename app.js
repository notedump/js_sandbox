// FUNCTION DECLARATIONS

function greet(firstName = 'John' , lastName = 'Doe') {
    // console.log('Hello');
    return 'Hello ' + firstName + ' ' + lastName;
}

// console.log(greet('Steve', 'Smith'));

// function greet(firstName, lastName){
//     if(typeof firstName === 'undefined'){firstName = 'John'}
//     if(typeof lastName === 'undefined'){lastName = 'Doe'}

//     return 'Hello ' + firstName + ' ' + lastName;
// }
// console.log(greet())


// FUNCTION EXPRESSIONS

const square = function(x = 5){
    return x*x;
};

// console.log(square());

// IMMIDIATELY INVOKABLE FUNCTION EXPRESSIONS - IIFEs

// (function() {
//     console.log('IIFE Ran..');
// })();

// (function(name) {
//     console.log('Hello ' +name);
// })('brad');

// PROPERTY METHODS

const todo = {
    add: function(){
        console.log('add to do..');
    },
    edit: function(id){
        console.log(`Edit todo ${id}`)
    }
} 

todo.delete = function(){
    console.log('Delete todo...');
}

todo.add();
todo.edit(22);
todo.delete('this');